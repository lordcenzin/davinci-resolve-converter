#!/usr/bin/python
# https://forum.blackmagicdesign.com/viewtopic.php?t=59346
# Transcode a video file to a format that Resolve for Linux can easily work with.
# mpeg4 video, pcm audio, 24 frames per second, keyframes every second,
# inside a qt container
#
# version 0.2

import os
from subprocess import call
import glob

import subprocess
import shlex
import json
import time

import argparse

debug = True

UNSUPPORTED_VIDEO_CODECS = ['h264']
UNSUPPORTED_AUDIO_CODECS = ['aac']


class Converter:
    def __init__(self, output_folder='./converted'):
        self.output_folder = output_folder

    # function to find the resolution of the input video file
    def find_video_audio_metadata(self, input_media):
        """
        return the name of the video and audio codec of the media file.
        :param input_media: the video to analyse
        :return: the metadata of teh media file.
        This is an example:
        'avg_frame_rate': '30/1',
                 'bit_rate': '3389543',
                 'chroma_location': 'left',
                 'codec_long_name': 'MPEG-4 part 2',
                 'codec_name': 'mpeg4',
                 'codec_tag': '0x7634706d',
                 'codec_tag_string': 'mp4v',
                 'codec_time_base': '1/30',
                 'codec_type': 'video',
                 'coded_height': 1080,
                 'coded_width': 1920,
                 'display_aspect_ratio': '16:9',
                 'disposition': { 'attached_pic': 0,
                                  'clean_effects': 0,
                                  'comment': 0,
                                  'default': 1,
                                  'dub': 0,
                                  'forced': 0,
                                  'hearing_impaired': 0,
                                  'karaoke': 0,
                                  'lyrics': 0,
                                  'original': 0,
                                  'timed_thumbnails': 0,
                                  'visual_impaired': 0},
                 'divx_packed': 'false',
                 'duration': '7.966667',
                 'duration_ts': 122368,
                 'has_b_frames': 0,
                 'height': 1080,
                 'index': 0,
                 'level': 1,
                 'max_bit_rate': '3389543',
                 'nb_frames': '239',
                 'pix_fmt': 'yuv420p',
                 'profile': 'Simple Profile',
                 'quarter_sample': 'false',
                 'r_frame_rate': '30/1',
                 'refs': 1,
                 'sample_aspect_ratio': '1:1',
                 'start_pts': 0,
                 'start_time': '0.000000',
                 'tags': { 'encoder': 'Lavc58.35.100 mpeg4',
                           'handler_name': 'VideoHandle',
                           'language': 'eng'},
                 'time_base': '1/15360',
                 'width': 1920},
               { 'avg_frame_rate': '0/0',
                 'bit_rate': '1536000',
                 'bits_per_sample': 16,
                 'channel_layout': 'stereo',
                 'channels': 2,
                 'codec_long_name': 'PCM signed 16-bit little-endian',
                 'codec_name': 'pcm_s16le',
                 'codec_tag': '0x74776f73',
                 'codec_tag_string': 'sowt',
                 'codec_time_base': '1/48000',
                 'codec_type': 'audio',
                 'disposition': { 'attached_pic': 0,
                                  'clean_effects': 0,
                                  'comment': 0,
                                  'default': 1,
                                  'dub': 0,
                                  'forced': 0,
                                  'hearing_impaired': 0,
                                  'karaoke': 0,
                                  'lyrics': 0,
                                  'original': 0,
                                  'timed_thumbnails': 0,
                                  'visual_impaired': 0},
                 'duration': '7.914667',
                 'duration_ts': 379904,
                 'index': 1,
                 'nb_frames': '379904',
                 'r_frame_rate': '0/0',
                 'sample_fmt': 's16',
                 'sample_rate': '48000',
                 'start_pts': 0,
                 'start_time': '0.000000',
                 'tags': {'handler_name': 'SoundHandle', 'language': 'eng'},
                 'time_base': '1/48000'}]}
        """
        VIDEO=0  # the index position where to find video information
        AUDIO=1  # the index position in the json where to find audio information
        cmd = "ffprobe -v quiet -print_format json -show_streams"
        args = shlex.split(cmd)
        args.append(input_media)
        # run the ffprobe process, decode stdout into utf-8 & convert to JSON
        ffprobe_output = subprocess.check_output(args).decode('utf-8')
        ffprobe_output = json.loads(ffprobe_output)

        # prints all the metadata available:
        # import pprint
        # pp = pprint.PrettyPrinter(indent=2)
        # pp.pprint(ffprobeOutput)

        # for example, find height and width
        # height = ffprobeOutput['streams'][0]['height']
        # width = ffprobeOutput['streams'][0]['width']
        video_codec_name = ffprobe_output['streams'][VIDEO]['codec_name']
        audio_codec_name = None
        if 'codec_name' not in ffprobe_output['streams'][AUDIO]:
            print('audio not in ffprobe output')
        else:
            audio_codec_name = ffprobe_output['streams'][AUDIO]['codec_name']
        return video_codec_name, audio_codec_name

    def convert_file(self, filename, overwrite=False):
        """
        convert a single input file in a mpeg4 format so that davinci resolve is recognizing it
        :param filename: the file to convert
        :return: the path of the new converted file
        """
        # Does file exist?
        if not os.path.exists(filename):
            print("\nERROR: Could not access \"" + filename + "\"  Exiting ...\n")
            exit()
        vc, ac = self.find_video_audio_metadata(filename)

        # if vc not in UNSUPPORTED_VIDEO_CODECS:
        #     print ('no need to convert')
        #     return filename
        # Set working directory to where file is located
        # os.chdir(os.path.dirname(filename_abs_path))
        # Strip file extension
        basefilename = os.path.splitext(os.path.basename(filename))[0] + "_conv.mov"
        outputfile = os.path.join(os.getcwd(), self.output_folder, basefilename)

        if debug:
            print("basefilename: {}".format(basefilename))
            print(filename)
            print(outputfile)

        # Does output directory exist? Create it.
        if not os.path.exists(self.output_folder):
            try:
                print('creating output folder: {}'.format(self.output_folder))
                os.mkdir(self.output_folder)
            except OSError:
                print("Could not create output directory. Exiting ...")
                exit()

        # The ffmpeg command
        # https://forum.blackmagicdesign.com/viewtopic.php?f=21&t=74922
        if os.path.exists(outputfile) and not overwrite:
            print('>>> {} already existing'.format(outputfile))
            skipped_file=outputfile
            return outputfile, skipped_file
        filename_abs_path = os.path.abspath(filename)
        cmd = "ffmpeg -y -i '{}' -c:v mpeg4 -q:v 8 -c:a pcm_s16le '{}'".format(filename_abs_path, outputfile)
        # Execute!!!
        skipped_file=''
        if debug:
            print(cmd)
        rc = call(cmd, shell=True)
        if rc != 0:
            skipped_file=filename_abs_path

        return outputfile, skipped_file

    def convert_folder(self, input_folder, overwrite=False):
        print('converting videos in folder {}'.format(input_folder))
        skipped=[]
        # Does output directory exist? Create it.
        if not os.path.exists(input_folder):
            print('missing folder {}'.format(input_folder))
            return -1
        for f in sorted(glob.iglob('{}/*'.format(input_folder))):
            if os.path.splitext(f)[1].lower() not in ['.mpeg', '.mpg', '.mp4', '.mov']:
                print('-- skipping format {}'.format(f))
                continue
            print('considering {}'.format(f))
            print('converting {}'.format(f))
            try:
                cf = self.convert_file(f, overwrite=overwrite)
                if len(cf[1]) > 0:
                    skipped.append(cf[1])
            except Exception as e:
                print('error while converting...\n{}'.format(e))
                print('** skipped {}'.format(f))
                skipped.append(f)
                time.sleep(5)
                continue
            print('converted {}'.format(cf))
            print('============================================')

        return skipped

def main():

    parser = argparse.ArgumentParser(description="convert mp4file parser")
    parser.add_argument("-i", "--inputfolder", help="the folder where video files are stored", required=True, default="")
    parser.add_argument("-o", "--outputfolder", help="the folder to use as output", required=False, default="")

    argument = parser.parse_args()
    status = False

    if argument.inputfolder:
        # print("You have used '-i' or '--save' with argument: {0}".format(argument.inputfolder))
        status = True
    if argument.outputfolder:
        # print("You have used '-o' or '--output' with argument: {0}".format(argument.outputfolder))
        status = True
    if not status:
        print("Maybe you want to use -H or -i or -o as arguments ?")

    input_folder = argument.inputfolder
    if len(argument.outputfolder) > 0:
        output_folder = argument.outputfolder
    else:
        output_folder = os.path.join(input_folder, "converted")
    print(input_folder, output_folder)

    conv = Converter(output_folder=output_folder)
    # converted_file = conv.convert_file(input_file)
    # print(converted_file)
    conv.convert_folder(input_folder)


if __name__ == "__main__":
    main()
