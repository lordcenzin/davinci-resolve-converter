import sys
import getopt
import glob
import time

from convert_mp4 import Converter

def usage():
    print("convert_mp4 <command> <option> \n\
    where command is \n\
    -i <video file_path> \n\
    -f <input_folder> \n\
    -o <output_folder>")
    print("Transcodes a video file or files within the current directory into a format that DaVinci Resolve for")
    print("Linux can easily use. Requires ffmpeg to be installed in user's path.")

def get_command(argv):
    inputfile = ''
    input_folder = ''
    output_folder = ''

    try:
        opts, args = getopt.getopt(argv[1:], "hi:f:o:", ["ifile=", "ifolder=", "ofile="])
    except getopt.GetoptError:
        print('{} -i <inputfile> -f <input_folder> -o <outputfolder>'.format(sys.argv[0]))
        sys.exit(2)
    print('opts', opts)
    print(args)
    for opt, arg in opts:
        if opt == '-h':
            print('{} -i <inputfile> -o <outputfile>'.format(sys.argv[0]))
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-f", "--ifolder"):
            print('setting folder')
            input_folder = arg
        elif opt in ("-o", "--ofile"):
            output_folder = arg
    if len(input_folder) == 0 and len(inputfile) == 0:
        input_folder = '.'  # by default considers current folder
    if len(output_folder) == 0:
        output_folder = './converted'  # by default creates a folder in the current folder

    print('Input file is {}'.format(inputfile))
    print('input folder is {}'.format(input_folder))
    print('Output folder is {}'.format(output_folder))
    return inputfile, input_folder, output_folder


def main(argv, debug=False):
    # If the script is called with no options or -h, print usage
    if len(sys.argv) == 1 or sys.argv[1] == '-h':
        usage()
        exit()

    if debug:
        print("sys.argv[0]: {}\nsys.argv[1]:{}".format(sys.argv[0], sys.argv[1]))
        # print ("os.path.dirname: {}".format(os.path.dirname(fn)))

    input_file, input_folder, output_folder = get_command(argv)

    overwrite=True
    # convert all mp4 files within the current directory
    conv = Converter()
    if input_file:
        print('converting just one file')
        res = conv.convert_file(input_file, overwrite=overwrite)
    else:
        print('converting a whole folder')
        res = conv.convert_folder(input_folder, overwrite=overwrite)
    print(res)

if __name__ == "__main__":
    main(argv=sys.argv, debug=True)