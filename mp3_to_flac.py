#!/usr/bin/python

import os
from subprocess import call
import glob
import argparse


def convert_file(fn, out_ext, output_folder, debug=True):
    # Does file exist?
    if os.path.exists(fn):
        fn = os.path.abspath(fn)
    else:
        print("\nERROR: Could not access \"" + fn + "\"  Exiting ...\n")
        exit()

    # Does output directory exist? Create it.
    if not os.path.exists(output_folder):
        try:
            print('creating output folder: {}'.format(output_folder))
            os.mkdir(output_folder)
        except OSError:
            print("Could not create output directory. Exiting ...")
            exit()

    # Set working directory to where file is located
    # os.chdir(os.path.dirname(fn))
    # Strip file extension
    basefilename = os.path.splitext(os.path.basename(fn))[0] + ".{}".format(out_ext)
    outputfile = os.path.join(output_folder, basefilename)

    if debug:
        print("basefilename: {}".format(basefilename))
        print(fn)
        print(outputfile)

    # The ffmpeg command
    # https://forum.blackmagicdesign.com/viewtopic.php?f=21&t=74922
    cmd = "ffmpeg -y -i '{}' '{}'".format(fn, outputfile)
    # Execute!!!
    if debug:
        print('\n----------------\n{}\n-------------\n'.format(cmd))
    call(cmd, shell=True)

def main():

    in_ext = 'mp3'
    out_ext = 'flac'

    parser = argparse.ArgumentParser(description="convert mp4file parser")
    parser.add_argument("-i", "--input", help="the file to convert or folder with files to convert", required=True, default="")
    parser.add_argument("-e", "--erase", help="if specified deletes the original file", required=False, action='store_true', default=False)
    parser.add_argument("-o", "--output", help="the output folder", required=False, default="")
    parser.add_argument("-d", "--debug", help="print more logs", required=False, default="True")

    argument = parser.parse_args()
    status = False

    if argument.input:
        print(">>>> input is: {0}".format(argument.input))
        status = True
    if argument.erase:
        print(">>>> erasing  original files...")
        status = True
    if argument.output:
        print('>>>> set as output folder:{}'.format(argument.output))
    if argument.debug:
        print(">>>> debug set as {}".format(argument.debug))
        status = True

    if not status:
        print("Maybe you want to use -H or -i or -o as arguments ?")

    if os.path.splitext(argument.input)[1].lower() not in ['.mp3', 'm4a']:
        # supposing it is a directory
        print('>>>> it is a directory')
        output_folder = os.path.join(argument.input, "converted")
        print(output_folder)
        if not os.path.exists(argument.input):
            print('missing folder {}'.format(argument.input))
            return -1
        for f in sorted(glob.glob('{}/*.{}'.format(argument.input, in_ext))):
            convert_file(f, out_ext, output_folder, argument.debug)
            if argument.erase:
                os.remove(f)
    else:
        # it is a single file
        output_folder = os.path.dirname(argument.input)
        print(output_folder)
        convert_file(argument.input, out_ext, output_folder, argument.debug)
        if argument.erase:
            os.remove(argument.input)


if __name__ == "__main__":
    main()